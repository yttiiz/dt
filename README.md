<div align="center">
    <img src="./images/dt_logo_lightblue.svg" width="50" height="auto" alt="dt logo" />
</div>

#

### Presentation
This is my [portfolio](https://www.dominiquetalis.com) repo code base. The first version came out in 2022. As a **motion-designer** & **front-end delevopper**, i wanna show my skills to anyone visit my website.

It was a very existing journey creating this. And it's not finish yet, cause it still work in progress.

## Stack
Obviously, the **holy trinity**, in the world of front-end development :
- _**HTML** 5_
- _**CSS** 3_
- _**Javascript** Esnext_

And a bit of _**php** 7_ to handle my contact form submission.

## Libraries 
- frond-end : just my personal with plain CSS & Vanilla Javascript
- back-end : _PhpMailer_ to handle sending emails

## Api 
- **Audius** : a free app, based on sharing music (like _spotify_ but organized around web3). I use the api to display my playlist
- **Google recaptcha** : the basic google recaptcha to prevent bot's message