<?php

$sep = DIRECTORY_SEPARATOR;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require 'PHPMailer' . $sep . 'src' . $sep . 'PHPMailer.php';
require 'PHPMailer' . $sep . 'src' . $sep . 'SMTP.php';
require 'MailFormatter.php';

class MailHandler
{    
    //Hostinger basics informations
    const HOSTINGER_HOST = 'smtp.hostinger.com';
    const HOSTINGER_ENCRYPTION = 'ssl';
    const HOSTINGER_PORT = 465;
    
    //Outlook basics informations
    const OUTLOOK_HOST = 'smtp-mail.outlook.com';
    const OUTLOOK_ENCRYPTION = 'tls';
    const OUTLOOK_PORT = 587;
    
    const ADMIN = 'Dominique Talis';
    const SUBJECT = 'Merci pour votre message';

    //File path
    const SEP = DIRECTORY_SEPARATOR;
    const DATA = '..' . self::SEP . 'register' . self::SEP . 'clients_info.txt';

    /**
     * Retrievals and sorting `form` data received.
     * @param array $post The content of the `$_POST` global variable.
     */
    public static function writeDataFile(array $post)
    {        
        $temp = [];

        //Exclude the 'g-recaptcha-response' token.
        foreach ($post as $key => $value) {
            if ($key === 'g-recaptcha-response') {
                continue;
            }

            $temp[$key] = $value;
        }

        $data = json_encode($temp, JSON_UNESCAPED_UNICODE);
        file_put_contents(self::DATA, $data . PHP_EOL, FILE_APPEND);
    }

    /**
     * Sends a message to the user.
     * @param PHPMailer $mail An instance of PHPMailer.
     */
    public static function sendEmailToUser($mail)
    {
        self::serverSettings(
            $mail,
            $_ENV['HOSTINGER_USERNAME'],
            $_ENV['HOSTINGER_PASSWORD'],
        );
        
        $userInfos = self::getUserInfo();

        //Recipients
        $mail->setFrom($_ENV['HOSTINGER_USERNAME'], self::ADMIN);
        $mail->addAddress($userInfos['email'], "{$userInfos['firstname']} {$userInfos['lastname']}");     //Add a recipient

        //Content
        $mail->Subject = self::SUBJECT;
        $mail->Body = MailFormatter::renderHTML($userInfos['firstname']);
        $mail->AltBody = MailFormatter::renderPlainText($userInfos['firstname']);

        $mail->send();
    }

    /**
     * Sends a message to the administrator.
     * @param PHPMailer $mail An instance of PHPMailer.
     */
    public static function sendEmailToAdmin($mail)
    {
        self::serverSettings(
            $mail,
            $_ENV['OUTLOOK_USERNAME'],
            $_ENV['OUTLOOK_PASSWORD'],
            self::OUTLOOK_HOST,
            self::OUTLOOK_PORT,
            self::OUTLOOK_ENCRYPTION
        );

        $userInfos = self::getUserInfo();

        //Recipients
        $mail->setFrom($_ENV['OUTLOOK_USERNAME'], 'NO-REPLY');
        $mail->addAddress($_ENV['HOSTINGER_USERNAME'], self::ADMIN);

        //Content
        $mail->Subject = 'Message de ' . $userInfos['firstname'] . ' ' . $userInfos['lastname'];
        $mail->Body = $userInfos['message'] .= !empty($userInfos['website']) ? '<br>Site web : ' . $userInfos['website'] : '';
        $mail->AltBody = 'Contenu du message : ' . $userInfos['message'] .= !empty($userInfos['website']) ? ' - Site web : ' . $userInfos['website'] : '';

        $mail->send();
    }

    /**
     * Email server settings configuration.
     * @param PHPMailer $mail An instance of PHPMailer.
     * @param string $username The SMTP username.
     * @param string $password The SMTP password.
     * @param string $host The SMTP server.
     * @param int $port The TCP port to connect to.
     * @param string $smtpSecure The SMTP secure.
     */
    private static function serverSettings(
        $mail,
        $username,
        $password,
        $host = self::HOSTINGER_HOST,
        $port = self::HOSTINGER_PORT,
        $smtpSecure = self::HOSTINGER_ENCRYPTION,
    ) {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                          //Enable verbose debug output
        $mail->isSMTP();                                                //Send using SMTP
        $mail->Host = $host;                                            //Set the SMTP server to send through
        $mail->SMTPAuth = true;                                         //Enable SMTP authentication
        $mail->Username = $username;                                    //SMTP username
        $mail->Password = self::isValid($password) ? $password : null;  //SMTP password
        $mail->SMTPSecure = $smtpSecure;                                //Enable implicit TLS encryption
        $mail->Port = $port;                                            //TCP port to connect to
        
        //Content
        $mail->isHTML();                                                //Set to HMTL
    }
    
    /**
     * Retrievals stored data.
     * @return array The user infos.
     */
    private static function getUserInfo(): array
    {
        $file = self::getFileLastEntry();
        
        return json_decode($file, true, 512, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Gets the last line of the 'data.txt' file.
     */
    private static function getFileLastEntry(): string
    {
        $dataStored = file(self::DATA);
        $index = count($dataStored) - 1;
        
        return $dataStored[$index];
    }

    /**
     * Checks if the given password is valid.
     * @param string $password The given password.
     */
    private static function isValid($password): bool
    {
        return password_verify($password, password_hash($password, PASSWORD_DEFAULT));
    }
}