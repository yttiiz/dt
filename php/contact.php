<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as PHPMailerException;
use Dotenv\Dotenv;

require_once realpath(__DIR__ . "/vendor/autoload.php");
require 'PHPMailer' . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . 'Exception.php';
require 'MailHandler.php';

$isPostEmpty = $_POST === [];
$isRecaptchaEmpty = $_POST['g-recaptcha-response'] === '';
$isSubmitBtnSet = (!empty($_POST['submit'])) && $_POST['submit'] != null;

if ($isPostEmpty || $isRecaptchaEmpty || $isSubmitBtnSet) {
    exit('Access denied');
}

//===================| Load .env variables |===================//

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

//===================| Check Google Recaptcha user response |===================//

$secret = $_ENV['RECAPTCHA_SECRET'];
$response = $_POST['g-recaptcha-response'];
$url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response";
$file = file_get_contents($url);
$result = json_decode($file, true);

if ($result['success']) {

    //===================| Sends mail with PHPMailer |===================//
    
    MailHandler::writeDataFile($_POST);
    
    try {
        MailHandler::sendEmailToUser(new PHPMailer(true));
        MailHandler::sendEmailToAdmin(new PHPMailer(true));
        
    } catch (PHPMailerException $e) {
        echo "Message could not be sent.";
    }
}