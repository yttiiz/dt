<?php

class MailFormatter {

    private static $header =
    '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <title>Message de Dominique Talis</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <style type="text/css">
            .lienmention { color:#333333; }
            /* Forcer l\'alignement du message dans hotmail */
            .ExternalClass { width:100%; }
            /* Pour &eacute;viter les changement de taille de texte sur mobile */
            body { -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
            /* Pour supprimer les bordure sur les tableaux */
            table td { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; margin:0; padding:0; }
    
            img { margin:0; padding:0; }
    
            @media only screen and (max-width: 3000px), (max-device-width: 3000px) {
                *[class~=general] { width:640px!important; }
                *[class~=p_desktop] { padding-left: 5px!important; padding-right: 5px!important; }
            }
    
            @media only screen and (max-width: 639px), (max-device-width: 639px) {
                *[class~=general] { width:320px!important; }
                *[class~=p_desktop] { padding-left: 0!important; padding-right: 0!important; }
    
                /* Modifier les dimentions en homoth&eacute;tie */
                *[class~=resize320] {
                    width:320px!important;
                    height:auto!important;
                }
                *[class~=resize270] {
                    width:270px!important;
                    height:auto!important;
                }
                *[class~=resize280] {
                    width:280px!important;
                    height:auto!important;
                }
    
                /* Modifier la largeur de fa&ccedil;on non homoth&eacute;tique */
                *[class~=w320] { width:320px!important; }
                *[class~=w318] { width:318px!important; }
                *[class~=w310] { width:310px!important; }
                *[class~=w300] { width:300px!important; }
                *[class~=w280] { width:280px!important; }
                *[class~=w40] { width:40px!important; }
                *[class~=w20] { width:20px!important; }
                *[class~=w10] { width:10px!important; }
    
                /* Modifier la hauteur de fa&ccedil;on non homoth&eacute;tique */
                *[class~=h19] { height:19px!important; }
    
                /* Supprimer le padding (ne fonctionne pas sous app gmail android => voir parades.txt) */
                *[class~=p0] { padding:0!important; }
    
                /* Supprimer le margin */
                *[class~=m0] { padding:0!important; }
    
                /* changer la taille de la typo (ne fonctionne pas sous app gmail android) */
                *[class~=f22] { font-size:22px!important; }
                *[class~=fcenter] { text-align:center!important; }
                *[class~=center] { margin:0 auto!important; }
                *[class~=none] { display:none; }
                *[class~=show] { display:block !important; margin: 0 auto; padding:0; overflow : visible !important; max-height:inherit !important; }
            }
        </style>
        <!--[if gte mso 9]>
            <xml>
            <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
            </o:OfficeDocumentSettings>
            </xml>
        <![endif]-->
    </head>
    ';

    private static $body =
    '<body text="#000000" style="background-color:#f6f6f7;">
    <center style="width:100%">
        <table width="100%" cellspacing="0" cellpadding="0" style="border:0; background-color:#f6f6f7">
            <tr>
                <td style="text-align:center; background-color:#f6f6f7">

                <!--[if (gte mso 9)|(IE)]>
                    <table cellpadding="0" cellspacing="0" width="600" style="width:600px; border:0; margin:auto" class="general"><tr><td align="left" valign="top">
                    <![endif]-->

                    <table cellspacing="0" cellpadding="0" width="600" style="width:600px; border:0; margin:auto" class="general">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0" style="border:0">
                                    <tr>
                                        <td height="40" style="height:40px; line-height:40px; font-size:1px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!-- Tableau general -->
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="w320" bgcolor="#FFFFFF">
                                    <!-- logo header -->
                                    <tr>
                                        <td>
                                            <table width="100%" bgcolor="#004ca3" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td>
                                                        <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="left" valign="top"><![endif]-->
                                                        <table style="width:100%;" align="left" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td height="20" style="height:20px; line-height:20px; font-size:1px;">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; padding-left:24px; padding-right:24px;">
                                                                    <a href="https://www.dominiquetalis.com" target="_blank" style="border:0;">
                                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <img src="https://dominiquetalis.com/images/dt_logo.png" width="50" height="59" style="display:block; border:0;" alt="dominique talis logo"/>
                                                                                </td>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0" border="0">
                                                                                        <tr>
                                                                                            <td align="left" style="padding: 0 0 3px 7px; font-family:Arial, Helvetica, sans-serif; font-size: 12px; color: #ffffff;">
                                                                                                <b>Dominique Talis</b>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" style="padding-left: 7px; font-family:Arial, Helvetica, sans-serif; font-size: 9px; line-height: 1; color: #ffffff;">
                                                                                                Designer polyvalent
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20" style="height:20px; line-height:20px; font-size:1px;">&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <!-- bloc presentation -->
                                    <tr>
                                        <td>
                                            <table width="100%" style="width:100%" align="left" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="padding:40px;">
                                                        <table role="presentation" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td align="left" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000 !important;">';
    
    private static $footer = 
                            '                                   </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="1" style="height:1px; line-height:1px; font-size:1px; background-color:#004ca3;" bgcolor="#004ca3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="padding: 10px;">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                                                <tr>
                                                    <td align="center" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color: #004ca3;">
                                                        <a href="https://fr.linkedin.com/in/dominique-talis-36a900124" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color: #004ca3;">Linkedin</a>
                                                        | 
                                                        <a href="https://www.youtube.com/channel/UCGlI54zYcPBY39AaWcZj55Q" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color: #004ca3;">Youtube</a>
                                                        | 
                                                        <a href="https://odysee.com/@dominiquetalis" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color: #004ca3;">Lbry</a>
                                                        | 
                                                        <a href="https://github.com/yttiiz" target="_blank" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color: #004ca3;">Gihub</a>
                                                    </td>
                                                </tr>   
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            <!-- END SNIP -->
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0" style="border:0">
                                    <tr>
                                        <td height="40" style="height:40px; line-height:40px; font-size:1px;">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                <!--[if (gte mso 9)|(IE)]>
                    </td></tr></table>
                <![endif]-->
            </td>
        </tr>
    </table>
</center>
</body>
</html>';
    

    public static function renderHTML(string $name): string
    {
        $header = self::$header;
        $body = self::$body;
        $footer = self::$footer;
        
        return <<<HTML
            $header
            $body
                <p style="margin: 0 0 20px; color:#000 !important">
                    Bonjour <strong>$name</strong>,
                </p>
                <p style="margin: 0 0 20px; color:#000 !important">
                    Je vous remercie, premi&egrave;rement, pour l&rsquo;int&eacute;r&ecirc;t que vous me portez et, deuxi&egrave;mement, pour avoir pris le temps de me laisser un mot. Je vous recontacte tr&egrave;s rapidement, apr&egrave;s m&rsquo;&ecirc;tre impr&eacute;gn&eacute; du contenu de votre message.
                </p>
                <p style="margin: 0; color:#000 !important">
                    Cordialement.
                </p>
            $footer
        HTML;
    }

    public static function renderPlainText(string $name): string
    {
        return "Bonjour $name,\nJe vous remercie, premi&egrave;rement pour l&rsquo;int&eacute;r&ecirc;ts que vous me portez et votre message. Je vous recontacte tr&egrave;s rapidement, apr&egrave;s m&rsquo;&ecirc;tre impr&eacute;gn&eacute; du contenu de votre message.\nCordialement.";
    }
}   
?>