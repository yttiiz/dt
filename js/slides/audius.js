import { AudioPlayer } from '../class/AudioPlayer.js'

//===================| les éléments / the elements |===================//

const [prevTrack, playTrack, nextTrack] = document.querySelectorAll('#player button')
const audioPlayer = new AudioPlayer('#player')

// //===================| les évènements / the events |===================//

audioPlayer.fetchDataFromAudiusApi()

playTrack.addEventListener('click', (e) => audioPlayer.playTrackHandler(e))
prevTrack.addEventListener('click', () => audioPlayer.prevTrackHandler())
nextTrack.addEventListener('click', () => audioPlayer.nextTrackHandler())
