import { Slider } from '../class/mod.js'
import {
    isViewportUnder850pixels,
    displaySelectedVisual,
    Modal
} from '../utils/mod.js'

//===================| The elements |===================//

/** @type {NodeListOf<HTMLButtonElement>} */
const [prev, next] = document.querySelectorAll('#print > ul li button')
const modal = document.getElementById('show-images')
const closeBtn = modal.querySelector('button')
const selector = '#print > div > div'

let sliderLength

isViewportUnder850pixels
    ? sliderLength = document.querySelector(selector).children.length / 2
    : sliderLength = document.querySelector(selector).children.length / 3

const slider = new Slider(selector, sliderLength)

//===================| The events |===================//

displaySelectedVisual(modal)

closeBtn.addEventListener('click', Modal.disparition)
window.addEventListener('click', Modal.disparitionViaWindow)

prev.addEventListener('click', () => slider.moveBack())
next.addEventListener('click', () => slider.moveForward())