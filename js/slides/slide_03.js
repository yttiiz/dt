import { fillSkillsLevel } from '../utils/mod.js'

//===================| The elements |===================//

export const [titleSkillGraphics, titleSkillDevelopement] = document.querySelectorAll('#competences ul li:nth-child(odd) h2')

/** @type {NodeListOf<HTMLSpanElement>} */
const skillsList = document.querySelectorAll('.notation')
const level = {
    photoshop: 9,
    illustrator: 9,
    inDesign: 8,
    afterEffects: 8,
    premiere: 8,
    animate: 6,
    js: 8,
    ts: 8,
    rust: 7,
    vue: 7,
    react: 8,
    deno: 7
}

//===================| The grades application |===================//

fillSkillsLevel(skillsList, level)