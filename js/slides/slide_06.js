import { Slider, Pitch, Playlist } from '../class/mod.js'

//===================| The elements |===================//

export const pitch = new Pitch('#football-teams > div:nth-child(2)')
export const playlist = new Playlist('#music-playlist')
const slider = new Slider('#code > div > div')

/** @type {NodeListOf<HTMLButtonElement>} */
const [prev, next] = document.querySelectorAll('#football-teams > div:nth-child(1) div button')
/** @type {NodeListOf<HTMLButtonElement>} */
const [playlistRankingBtn, playlistReloadBtn] = document.querySelectorAll('#music-playlist div button')
/** @type {NodeListOf<HTMLButtonElement>} */
const [prevSlider, nextSlider] = document.querySelectorAll('#code > ul li button')

//===================| The events |===================//

//Football Teams

next.addEventListener('click', (e) => {
    if (pitch.count >= 3) return
    
    pitch.incrementCount()
    pitch.setPitch(e)
})

prev.addEventListener('click', (e) => {
    if (pitch.count <= 1) return
    
    pitch.decrementCount()
    pitch.setPitch(e)
})

//Playlist Music

playlistRankingBtn.addEventListener('click', () => playlist.handleRanking())
playlistReloadBtn.addEventListener('click', () => playlist.reloadRanking())

//Slider

prevSlider.addEventListener('click', () => slider.moveBack())
nextSlider.addEventListener('click', () => slider.moveForward())