import { Handler } from '../utils/mod.js'

//===================| The elements |===================//

/** @type {NodeListOf<HTMLSpanElement>} */
const [textShowArrows, textShowLandmarks] = document.querySelectorAll('#presentation span')

//===================| The events |===================//

if (!(window.innerWidth <= 850)) {
    textShowArrows.addEventListener('mouseenter', Handler.arrows)
    textShowArrows.addEventListener('mouseleave', Handler.arrows)
    
    textShowLandmarks.addEventListener('mouseenter', Handler.landmarks)
    textShowLandmarks.addEventListener('mouseleave', Handler.landmarks)
}