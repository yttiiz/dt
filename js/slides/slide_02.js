import { Slider } from '../class/mod.js'

//===================| The elements |===================//

/** @type {NodeListOf<HTMLButtonElement>} */
const [prev, next] = document.querySelectorAll('#parcours > ul li button')
const selector = '#parcours > div > div'
const sliderLength = document.querySelector(selector).children.length / 2
const slider = new Slider(selector, sliderLength)

//===================| The events |===================//

prev.addEventListener('click', () => slider.moveBack())
next.addEventListener('click', () => slider.moveForward())