import { Form } from '../class/mod.js'

//===================| The element |===================//

export const form = new Form('#contact form')

//===================| The event |===================//

form.element.addEventListener('submit', (e) => form.handleSendingData(e))