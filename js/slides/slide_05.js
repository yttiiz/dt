import { Slider, VideoPlayers } from '../class/mod.js'

//===================| The elements |===================//

/** @type {NodeListOf<HTMLButtonElement>} */
const [prev, next] = document.querySelectorAll('#motion > ul li button')
const slider = new Slider('#motion > div > div')
export const players = new VideoPlayers('#motion .video-container')

//===================| The events |===================//

prev.addEventListener('click', () => slider.moveBack())
next.addEventListener('click', () => slider.moveForward())

players.fetchVideos()