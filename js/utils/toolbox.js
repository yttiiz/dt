//===================| Generics |===================//

/**
 * Creates an img and selects the button of the figure.
 * @param {HTMLElement} modal A `figure` element. 
 * @returns {[HTMLImageElement, HTMLButtonElement]} An array with an `img` and a `button`.
 */
export function createImgAndSelectMainFigureBtn(modal) {
    const img = document.createElement('img')
    const closeBtn = modal.querySelector('button')

    return [img, closeBtn]
}

/**
 * Insert the img before the button in the figure container.
 * @param {HTMLElement} modal A `figure` elemnent.
 * @param {HTMLImageElement} img A `img` element.
 * @param {HTMLButtonElement} closeBtn A `button` element.
 */
export function insertImgIntoFigure(modal, img, closeBtn) {
    modal.querySelector('div').insertBefore(img, closeBtn)
    modal.style.display = 'flex'
}

//===================| Slide 3 |===================//

/**
 * Fills all the points that respect the level skill, with the 'niveau' class.  
 * @param {HTMLSpanElement} el 
 * @param {number} note 
 */
function skillsLevel(el, note, i = 0) {
    for (const point of el.children) {
        if (i + 1 > note) return
        point.classList.add('niveau')
        i++
    }
}

/**
 * Iterates thougthout the `points` spans and fills them with the corresponding notation. 
 * @param {NodeListOf<HTMLSpanElement>} skills A list of spans.
 * @param {Object} level Object skills notation.
 * @param {number} i Iterator.
 */
export function fillSkillsLevel(skills, level, i = 0) {
    for (const key in level) {
        skillsLevel(skills[i], level[key])
        i++
    }
}

//===================| Slide 4 |===================//

/**
 * Displays the selected `img`, provided by a click event on a div in the nodelist, into the `figure` element.
 * @param {HTMLElement} modal The `figure` element.
 * @param {NodeListOf<HTMLDivElement>} divs The default nodelist of divs which contains `img`.
 */
export function displaySelectedVisual(
    modal = document.querySelector('#show-images'),
    divs = document.querySelectorAll('#print > div > div > div > div')
) {
    divs.forEach((node, index) => {
        node.addEventListener('click', (e) => {
            const [img, closeBtn] = createImgAndSelectMainFigureBtn(modal)
            
            img.src = `./images/flyer_0${index + 1}.jpg`
            img.alt = node.querySelector('img').alt

            insertImgIntoFigure(modal, img, closeBtn)
            e.stopPropagation()
        })
    })
}