const SECONDS = 1, MINUTES = 60 * SECONDS, HOURS = 60 *  MINUTES
const times = {
    hours: HOURS,
    minutes: MINUTES
}

/**
 * Converts the duration media (in seconds) into hours, minutes and seconds. 
 * @param {number} x The duration of the media.
 */
const timeCalculation = (x) => {
    const timeInit = {
        hours: 0,
        minutes: 0,
        seconds: 0
    }
    let timeToDisplay = ''

    x = Math.abs(x)

    if (isNaN(x)) {
        timeToDisplay = '00:00'
        return timeToDisplay
    }

    Object.keys(times).forEach(k => {
        timeInit[k] = Math.floor(x / times[k])
        x -= timeInit[k] * times[k]

        if (k === 'hours' && timeInit[k] === 0) {
            timeToDisplay += `${timeInit[k] > 0 ? timeInit[k] < 10 ? `0${timeInit[k]}:` : `${timeInit[k]}:` : ''}`

        } else {
            timeToDisplay += `${timeInit[k] > 0 ? timeInit[k] < 10 ? `0${timeInit[k]}` : `${timeInit[k]}` : '00'}:`
        }
    })

    timeInit.seconds = Math.round(x / SECONDS)

    timeToDisplay += `${timeInit.seconds > 0 ? timeInit.seconds < 10 ? `0${timeInit.seconds}` : `${timeInit.seconds}` : '00'}`
    
    return timeToDisplay
}

/**
 * Returns the current time of the media.
 * @param {HTMLMediaElement} el 
 */
function showElementCurrentTime(el) {
    return timeCalculation(Math.round(el.currentTime))
}

/**
 * Returns the fulltime of the media.
 * @param {HTMLMediaElement} el 
 */
function showElementFulltime(el) {
    return timeCalculation(Math.round(el.duration))
}

/**
 * Display the current and full time of a given `video`.
 * @param {Event} e Media event.
 */
export function displayTimer(e) {
    if (e.currentTarget instanceof HTMLMediaElement) {
        const span = e.currentTarget.parentNode.querySelector('.btns-container span')

        if (e.currentTarget.readyState >= 1) {
            span.innerHTML = `<span>${showElementCurrentTime(e.currentTarget)}</span><span> / </span><span>${showElementFulltime(e.currentTarget)}</span>`
        }
    }
}

/**
 * Display the current and full time of a given `audio`.
 * @param {HTMLSpanElement} span A span container.
 * @param {HTMLMediaElement} audio An audio provided by Audius.
 */
export function displayAudiusTimer(span, audio) {
    span.textContent = `${showElementCurrentTime(audio)} / ${showElementFulltime(audio)}`
}