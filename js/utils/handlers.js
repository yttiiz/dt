
// import assertions are not currently supported
// import data from '../../json/data.json' assert { type: 'json' }
import * as Type from '../types/types.js'

const data = await fetch('../../json/data.json')
.then(data => data.json())
.then(data => data)

//===================| Export data for classes |===================//

export const dt = data

//===================| Responsive Checker |===================//

export const isViewportUnder850pixels = window.innerWidth <= 850 

//===================| Handlers |===================//

export class Handler {
    /**
     * Animates the lines of the burger and display or not the menu.
     * @param {Event} e A 'click' event.
     */
    static burger(e) {
        if (e.currentTarget instanceof HTMLButtonElement) {
            let key = 0
            
            for (const line of e.currentTarget.querySelectorAll('span')) {
                key++
                line.classList.toggle(`line${key}`)
            }
            // menu
            e.currentTarget.nextElementSibling
            .classList.toggle('show-menu')
        }
    }

    /**
     * Handles the behavior of the arrows.
     * @param {Event} e Event
     * @param {NodeListOf<HTMLButtonElement>} arrows Main arrows list.
     */
    static arrows(
        e,
        arrows = document.querySelectorAll('#main-container > button')
    ) {
        arrows.forEach((btn, index) => {
            switch(e.type) {
                case 'mouseenter':
                    if (index === 0) btn.classList.add('button-move-left')
                    if (index === 1) btn.classList.add('button-move-right')
                    break

                default:
                    if (index === 0) btn.classList.remove('button-move-left')
                    if (index === 1) btn.classList.remove('button-move-right')        
            }
        })
    }

    /**
     * Handles the behavior of the landmarks.
     * @param {Event} e Event
     * @param {HTMLSpanElement} landmarks The landmarks container.
     */
    static landmarks(
        e,
        landmarks = document.querySelector('#main-container > span')
    ) {
        switch(e.type) {
            case 'mouseenter':
                landmarks.querySelector('ul').classList.add('landmarks-move')
                break

            default:
                landmarks.querySelector('ul').classList.remove('landmarks-move')
        }
    }

    /**
     * Handles the paragraphs selected translation.
     * @param {string} lang The current language.
     * @param {Type.PresentationDataType} info A French | English object traduction.
     * @param {NodeListOf<HTMLParagraphElement>} paragraphs A nodelist of paragraphs.
     */
    static paragraph(
        lang = 'fra',
        info = dt.find(obj => obj.id === 'Presentation'),
        paragraphs = document.querySelectorAll('#presentation p')
    ) {
        const [mainTextPart1, mainTextPart2] = paragraphs
    
        mainTextPart1.innerHTML = info.data.traduction[lang][1]
        mainTextPart2.childNodes.forEach((node, index) => {
            isViewportUnder850pixels
            ? node.textContent = info.data.traduction[lang][3][index]
            : node.textContent = info.data.traduction[lang][2][index]
        })
    }
    
    /**
     * Handles the responsive breakpoint (less than 850 pixels) to show the right title in the right slide. 
     * @param {Event | undefined} _ A 'resize' event.
     * @param {Type.TitlesType} slidesTitles A French | English object traduction.
     * @param {HTMLButtonElement} traductionBtn The default traduction button.
     * @param {NodeListOf<HTMLElement>} slides All the slides of the slider.
     */
    static breakPoint(
        _ = undefined,
        slidesTitles = dt.find(obj => obj.id === 'Titles'),
        traductionBtn = document.querySelector('nav > button'),
        slides = document.querySelectorAll('.slide')
    ) {
        const icones = document.querySelectorAll('.icone-play')
        const lang = traductionBtn.textContent.trim()
        
        if (window.innerWidth <= 850) {
            const svgPlay = `<svg x="0px" y="0px" viewBox="0 0 20 20"><polygon points="2,2 2,18 18,10 "/></svg>`
            
            //Move the main slider to its initial x asis position
            //And reset the main title
            /** @type {HTMLElement} */
            const slider = document.querySelector('#slider')

            if (slider.style.transform) {
                slider.style.transform = 'unset'
                document.querySelector('h1').textContent = lang === 'fra'
                ? slidesTitles.data.traduction['eng'][0]
                : slidesTitles.data.traduction['fra'][0]
            }
            
            icones.forEach(icone => icone.innerHTML = svgPlay)
            slides.forEach((slide, index) => {
                //Excludes first slide "Presentation"
                if (index !== 0) {
    
                    lang === 'fra'
                    ? slide.dataset.title = slidesTitles.data.traduction['eng'][index]
                    : slide.dataset.title = slidesTitles.data.traduction['fra'][index]
                }
            })
    
        } else {
    
            icones.forEach(icone => icone.innerHTML = '')
            slides.forEach((slide, index) => {
                //Excludes first slide "Presentation"
                if (index !== 0) {
                    slide.dataset.title = ''
                }
            })
        }
    
        lang === 'fra'
        ? Handler.paragraph('eng')
        : Handler.paragraph()
    }

    //===================| Slide 1 |===================//

    /**
     * Switches those given HTML elements content in the current query language `French or English`.
     * @param {Object} info A French | English object traduction. 
     * @param {HTMLButtonElement} languageBtn The translation button. 
     */
    static presentationTextContent(
        info = dt.find(obj => obj.id === 'Presentation'),
        languageBtn = document.querySelector('nav > button'),
    ) {
        /**
         * Fills the children container text content according to the given language argument. 
         * @param {string} lang The selected language.
         * @param {HTMLCollection} jobs The `ul` children collection .
         * @param {number} i The index to set the current child in the collection.
         */
        const handleLanguage = (
            lang = 'fra',
            jobs = document.querySelector('#presentation ul').children,
            i = 0
        ) => {
            for (const job of jobs) {
                job.textContent = info.data.traduction[lang][0][i]
                i++
            }

            Handler.paragraph(lang)
        }

        languageBtn.textContent === 'fra' ? handleLanguage('eng') : handleLanguage()  
    }

    //===================| Slide 2 |===================//

    /**
     * Switch HTML elements text content in the current query language `French or English`.
     * @param {NodeListOf<HTMLDivElement>} cards A list of div containers.
     * @param {Type.CareerDataType} info A French | English array traduction.
     * @param {HTMLButtonElement} languageBtn The default translation button.
     */
    static careerTextContent(
        cards = document.querySelectorAll('#parcours > div > div > div'),
        info = dt.find(obj => obj.id === 'Career'),
        languageBtn = document.querySelector('nav > button')
    ) {
        const compagnies = ['imagine', 'yz', 'laposte-ppdc', 'laposte-marketing', 'matacapital', 'yz-tech']
        /**
         * Fills the text content of the children container.
         * @param {HTMLDivElement} container 
         * @param {string[]} arr 
         */
        const fillChildrenContainerTextContent = (container, arr) => {
            const [h2, span, p] = arr
            container.querySelector('h2').textContent = h2
            container.querySelectorAll('span')[1].textContent = span
            container.querySelector('p').innerHTML = p
        }

        /**
         * Fills the children container text content according to the given language argument. 
         * @param {string} lang The selected language.
         */
        const handleLanguage = (lang = 'fra', i = 0) => {
            for (const div of cards) {
                fillChildrenContainerTextContent(
                    div,
                    info.data.traduction[lang][compagnies[i]]
                )
                i++
            }
        }
            
        languageBtn.textContent === 'fra' ? handleLanguage('eng') : handleLanguage()
    }

    //===================| Slide 3 |===================//

    /**
     * Switch HTMLElements content in the current query language `French or English`.
     * @param {NodeListOf<HTMLHeadingElement>} headings Graphics & developpement heading elements of the section.
     * @param {Type.SkillsDataType} info A French | English array traduction.
     * @param {HTMLButtonElement} languageBtn The default translation button.
     */
    static skillsTextContent(
        headings = document.querySelectorAll('#competences ul li:nth-child(odd) h2'),
        info = data.find(obj => obj.id === 'Skills'),
        languageBtn = document.querySelector('nav > button')
    ) {
        const [graf, dev] = headings    
        graf.textContent = languageBtn.textContent === 'fra' ? info.data.traduction.eng[0] : info.data.traduction.fra[0]
        dev.textContent = languageBtn.textContent === 'fra' ? info.data.traduction.eng[1] : info.data.traduction.fra[1]
    }
}

export class Modal {

    /**
     * Removes the img in the 'figure'.
     * @param {HTMLDivElement} container The `div` container of the figure.
     * @param {HTMLElement} modal The `figure` element.
     */
    static removeImg(container, modal) {
        const img = modal.querySelector('img'), closeBtn = modal.querySelector('button')
    
        container.removeChild(img)
        modal.style.display = 'none'

        if (
            'dataset' in modal.firstElementChild &&
            modal.firstElementChild.dataset instanceof Object &&
            'infosName' in modal.firstElementChild.dataset &&
            'infosNumber' in modal.firstElementChild.dataset
        ) {
            modal.firstElementChild.dataset.infosName = ''
            modal.firstElementChild.dataset.infosNumber = ''
        }
    
        if (closeBtn.classList.contains('center-btn')) {
            closeBtn.classList.remove('center-btn')
        }
    }

    /**
     * Removes the image in the `figure` element and changes its display mode to 'none'. 
     * @param {Event} e A 'click' event.
     */
    static disparition(e) {
        if (e.currentTarget instanceof HTMLButtonElement) {

            /** @type {HTMLElement} */
            const modal = e.currentTarget.closest('figure')
            const container = modal.querySelector('div')
            Modal.removeImg(container, modal)
        }
    }
    
    /**
     * Checks the event target and if it's not coming from the image, the `figure` changes its display mode to 'none'. It's the simulation of a click out of the box to close the modal. 
     * @param {Event} e A 'click' event.
     * @param {HTMLElement} modal A `figure` element which shows the images selected.
     */
    static disparitionViaWindow(
        e,
        modal = document.getElementById('show-images')
    ) {
        /** @type {Object} */
        const target = e.target
        const div = modal.querySelector('div')

        if ('dataset' in target &&
            'name' in target.dataset && (
                target.dataset.name === 'svg-close' ||
                target.dataset.name === 'circle-close' ||
                target.dataset.name === 'polygon-close'
            )
        ) return
    
        if (
            div.firstElementChild.nodeName === 'IMG' &&
            target?.nodeName === 'FIGURE'
        ) {
            Modal.removeImg(div, modal)
        }
    }
}







