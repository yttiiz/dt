import { MainSlider } from './class/mod.js'
import { Handler } from './utils/mod.js'
import {
    pitch,
    playlist,
    form
} from './slides/mod.js'

//===================| The elements |===================//

const traductionBtn = document.querySelector('nav > button')
const infosWarning = document.querySelector('#player > span')
const burger = document.querySelector('nav > div button')
const slider = new MainSlider(
    '#slider',
    document.querySelectorAll('#main-container > button')
)

//===================| The events |===================//

//Window resizing
window.onresize = Handler.breakPoint
Handler.breakPoint()

//Language switching
traductionBtn.addEventListener('click', () => {
    infosWarning.textContent = ''
    slider.setCopyrightAndArrowsNavTextContent()
    Handler.breakPoint()
    Handler.presentationTextContent()
    Handler.careerTextContent()
    Handler.skillsTextContent()
    pitch.setAllTextContent()
    playlist.setAllTextContent()
    form.setAllTextContent()
})

//Burger animation
burger.addEventListener('click', Handler.burger)

//Window HTML loaded
window.addEventListener('DOMContentLoaded', () => Handler.paragraph())