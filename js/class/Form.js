import { dt } from '../utils/mod.js'
import * as Type from '../types/types.js'

export class Form {
    /** @type {HTMLFormElement} */
    #element
    /** @type {HTMLDialogElement} */
    #dialog
    /** @type {Type.FormDataType['data']} */
    #data

    /**
     * @param {`#${string} form`} selector The query string.
     */
    constructor(selector) {
        this.#element = document.querySelector(selector)
        this.#dialog = document.querySelector("#mail-modal")
        this.#data = dt.find(obj => obj.id === 'Form').data
        this.#initDialog()
    }

    get element() {
        return this.#element
    }

    #initDialog() {
        for (const btn of this.#dialog.querySelectorAll("button")) {
            btn.addEventListener("click", (e) => {
                if (e.currentTarget instanceof HTMLButtonElement) {
                    e.currentTarget.closest("dialog").close()
                }
            })
        }
    }

    /**
     * @param {string} title 
     * @param {string} paragraph 
     */
    #setModal(title, paragraph) {
        this.#dialog.querySelector("h2").textContent = title
        this.#dialog.querySelector("p").textContent = paragraph
        this.#dialog.showModal()
    }

    /**
     * @param {HTMLButtonElement} languageBtn The default translation button.
     */
    #contactTextChanges(languageBtn = document.querySelector('nav > button')) {
        const paragraph = this.#element.previousElementSibling
        const [firstname, lastname, _, website] = this.#element.querySelectorAll('input')
        const message = this.#element.querySelector('textarea')
        
        /**
         * @param {string} lang 
         * @param {(HTMLInputElement | HTMLTextAreaElement)[]} els 
         */
        const fillTextContent = (lang, ...els) => {
            for (const el of els) {
                el.placeholder = this.#data.traduction[lang][el.name]
            }
        }
        
        /**
         * @param {'fra' | 'eng'} lang 
         */
        const handleLanguage = (lang = 'fra') => {
            paragraph.textContent = this.#data.traduction[lang].text
            fillTextContent(lang, firstname, lastname, website, message)
        }

        languageBtn.textContent === 'fra'
        ? handleLanguage('eng')
        : handleLanguage()
    }

    /**
     * @param {HTMLButtonElement} languageBtn The default translation button.
     */
    #contactValidationInfosChanges(languageBtn = document.querySelector('nav > button')) {
        /** @type {HTMLSpanElement} */
        const required = this.#element.querySelector('span[data-required]')
        
        /** @type {HTMLInputElement} */
        const submit = this.#element.querySelector('input[type="submit"]')
        
        /**
         * @param {'fra' | 'eng'} lang 
         */
        const handleLanguage = (lang = 'fra') => {
            required.textContent = this.#data.traduction[lang].fieldsRequired
            submit.value = this.#data.traduction[lang].button
        }

        languageBtn.textContent === 'fra'
        ? handleLanguage('eng')
        : handleLanguage()
    }

    /**
     * @param {string} firstname 
     * @param {string} successMsg 
     * @param {string} errorRequestMsg 
     */
    async #sendClientInfos (
        firstname,
        successMsg,
        errorRequestMsg
    ) {
        const email = 'contact@dominiquetalis.com'
        const contentDialogSuccessMessage = () => `${firstname}, ${successMsg}`
        const titleDialogMessage = (status = 200) => {
            const isFrenchTranslation = document.querySelector('nav > button').textContent.trim() === "eng"
            return status === 200 
            ? `Message ${isFrenchTranslation ? "envoyé" : "sent"}`
            : `Message ${isFrenchTranslation ? "non envoyé" : "not sent"}`
        }
        
        const formData = new FormData(this.#element)
        const dateNow = Intl.DateTimeFormat("fr-FR", {
            dateStyle: "full",
            timeStyle: "long"
        }).format(Date.now())

        formData.append("sentAt", dateNow)

        const res = await fetch(this.#element.action, {
            method: 'POST',
            body: formData
        })

        switch(res.status) {
            case 200: 
                this.#setModal(titleDialogMessage(), contentDialogSuccessMessage())
                break

            default:
                this.#setModal(titleDialogMessage(res.status), `${errorRequestMsg} : ${email}`)
        }
    }

    /**
     * Sets the entire text content of the current slide.
     */
    setAllTextContent() {
        this.#contactTextChanges()
        this.#contactValidationInfosChanges()   
    }

    /**
     * @param {SubmitEvent} e A 'submit' event.
     * @param {HTMLButtonElement | null} languageBtn The default translation button.
     */
    handleSendingData(
        e,
        languageBtn = document.querySelector('nav > button')
    ) {
        e.preventDefault()
        
        if (e.target instanceof HTMLFormElement) {
            const form = e.target

            /** @type {NodeListOf<HTMLInputElement>} */
            const fields = form.querySelectorAll('input:not([type=submit])')
            const spanRequired = form.querySelector('span')
            const [firstName, lastName, ] = fields
            const regex = /^[a-zA-Z-çéèêôûëöü\s]+$/
            const lang = languageBtn.textContent === 'fra'
            ? 'eng'
            : 'fra'
                    
            //Handle responsive
            window.innerWidth <= 500
            ? spanRequired.style.marginBottom = '50px'
            : null
            
            let isOneOrMoreFieldsEmpty = false
            
            /** @type {object} */
            let captcha
            "grecaptcha" in window ? captcha = window.grecaptcha : null
            
            /**
             * @param {HTMLInputElement} field 
             */
            const isValid = (field) => regex.test(field.value) === false

            /**
             * @param {'isEmpty' | 'clear'} action
             */
            const makeActionOverFields = (action) => {
                for (const field of form.children) {
                    const isFieldIsAnInputText =
                    field instanceof HTMLInputElement ||
                    field instanceof HTMLTextAreaElement;

                    switch(action) {
                        case 'isEmpty': {
                            if (isFieldIsAnInputText && field.name && field.name !== 'website') {
                                if (field.value === '') {
                                    isOneOrMoreFieldsEmpty = true
                                    break 
                                }
                            }
                            break 
                        }

                        case 'clear': {
                            isFieldIsAnInputText && field.name ? field.value = '' : null
                            break
                        }
                    }
                }
                
            }

            makeActionOverFields('isEmpty')
            
            if (isOneOrMoreFieldsEmpty) {
                spanRequired.attributes['data-required'].value = (
                    this.#data.traduction[lang].errorRequiredMessage
                )
                
            } else if (isValid(firstName) || isValid(lastName)) {
                spanRequired.attributes['data-required'].value = (
                    this.#data.traduction[lang].errorCharacterMessage
                )
                
            } else {
                spanRequired.attributes['data-required'].value = ''

                //Verify if the recaptcha have been checked
                if ('getResponse' in captcha && !captcha.getResponse()) {
                    this.#setModal(
                        lang === "eng" ? "Erreur recaptcha" : "Recaptcha error",
                        this.#data.traduction[lang].errorRecaptchaMessage
                    )
                    return
                }

                this.#sendClientInfos(
                    firstName.value,
                    this.#data.traduction[lang].successMessage,
                    this.#data.traduction[lang].errorRequestMessage
                )

                makeActionOverFields('clear')
            }
        }
    
    }
}