import {
    dt,
    createImgAndSelectMainFigureBtn,
    insertImgIntoFigure
} from '../utils/mod.js'
import * as Type from '../types/types.js'

export class Pitch {
    /** @type {HTMLDivElement} */
    #element
    #mainInfosContainer
    #detailsInfosContainer
    #grid
    #positions
    #index
    /** @type {Intl.DateTimeFormatOptions} */
    #optsDate
    /** @type {Type.PitchDataType} */
    #data
    
    /**
     * Creates an object to handle the behavior of the 'football pitch'.
     * @param {`#${string}`} selector The quey string.
     */
    constructor(selector) {
        this.#element = document.querySelector(selector)
        this.#data = dt.find(obj => obj.id === 'Pitch').data
        this.#mainInfosContainer = this.#element.previousElementSibling
        this.#detailsInfosContainer = this.#element.nextElementSibling
        this.#grid = this.#element.querySelector('div')
        this.#positions = [...this.#grid.querySelectorAll('div')]
        this.#index = 0
        this.#optsDate = {
            year: 'numeric',
            month: 'short',
            day: 'numeric'
        };
        this.#showMarks()
    }

    get count() {
        return this.#index
    }

    incrementCount() {
        this.#index++
    }

    decrementCount() {
        this.#index--
    }

    #getBirthDate([year, month, day], locale = 'fr-FR') {
        const date = new Date(Date.UTC(year, month, day))

        return Intl.DateTimeFormat(locale, this.#optsDate).format(date)
    }

    /**
     * Shows the index of the current team in the list. 
     */
    #showMarks() {
        const mark = this.#mainInfosContainer.querySelector('mark')
        mark.textContent = `${this.#index}/${this.#data.teams.length}`
    }

    /**
     * Shows the details (photo, name, place and birth) of the selected player.
     * @param {Event} e The default translation button.
     * @param {HTMLButtonElement} traductionBtn The default translation button.
     */
    #showPlayer(
        e,
        traductionBtn = document.querySelector('nav > button')
    ) {
        this.#positions.forEach((position, index) => {
            const i = this.#positions.length - 1
            const playerContainer = this.#detailsInfosContainer.querySelector('div:nth-child(3)')
            const playerPhoto = playerContainer.querySelector('img')
            const [playerName, playerPlace, playerBirth] = playerContainer.querySelectorAll('li')

            if (this.#positions[index].id === this.#data.teams[this.#index - 1].players[i - index].identity) {
                const currentPlayer = this.#data.teams[this.#index - 1].players[i - index]

                position.addEventListener('click', () => {
                    
                    if (window.innerWidth <= 650) {
                        /**
                         * Render the image of the current player with his infos.
                         * @param {HTMLImageElement} img 
                         * @param {HTMLButtonElement} closeBtn 
                         * @param {Type.PlayerType} currentPlayer 
                         */
                        const renderImg = (img, closeBtn, currentPlayer) => {
                            const playerAllNames = `${currentPlayer.firstname} ${currentPlayer.lastname}`
                            
                            img.style.objectPosition = currentPlayer.photo
                            img.dataset.name = 'player'
                            img.src = './images/sprite_teams_players.png'
                            img.alt = playerAllNames

                            const div = closeBtn.closest('div')
                            div.setAttribute('data-name', 'player-container')
                            div.setAttribute('data-infos-name', playerAllNames)
                            div.setAttribute('data-infos-number', `${currentPlayer.number}`)

                            if (!closeBtn.classList.contains('center-btn')) {
                                closeBtn.classList.add('center-btn')
                            }
                        }
                        /** @type {HTMLDivElement} */
                        const modal = document.querySelector('#show-images')
                        let img, closeBtn

                        //If the image have not been yet created.
                        if (!modal.querySelector('img')) {
                            [img, closeBtn] = createImgAndSelectMainFigureBtn(modal)
                            renderImg(img, closeBtn, currentPlayer)
        
                            insertImgIntoFigure(modal, img, closeBtn)

                        } else {
                            //Else select the img.
                            img = modal.querySelector('img')
                            closeBtn = modal.querySelector('button')
                            renderImg(img, closeBtn, currentPlayer)
                        }
                        
                        e.stopPropagation()
    
                    } else {
                        const {
                            firstname,
                            lastname,
                            number,
                            photo,
                            place,
                            poste,
                            year,
                            month,
                            day
                        } = currentPlayer

                        playerContainer.setAttribute('data-num', `${number}`)
                        playerPhoto.style.objectPosition = photo
                        playerName.textContent = `${firstname} ${lastname}`
        
                        if (traductionBtn.textContent === 'fra') {
                            playerPlace.textContent = place
                            playerBirth.textContent = `Born in ${this.#getBirthDate([year, month, day], 'en-EN')}`
                            
                            
                        } else {
                            playerPlace.textContent = poste
                            playerBirth.textContent = `Né le ${this.#getBirthDate([year, month, day])}`
                        }
                    }
                })
            
            }
        })
    }

    /**
     * Displays the players infos (name, number) on the pitch, according to the team system and its color.
     */
    #displayTeam() {
        const [logo, name] = [...this.#detailsInfosContainer.querySelector('div:nth-child(1)').children]
        /** @type {NodeListOf<HTMLSpanElement>} */
        const [system, manager, captain] = this.#detailsInfosContainer.querySelectorAll('div:nth-child(2) li span')
        
        if (this.#index > 0) this.#grid.style.display = 'grid'

        this.#data.teams.forEach((team, index) => {
            
            if (this.#index === (index + 1)) {
                if (logo instanceof HTMLImageElement) {
                    logo.style.objectPosition = team.logo
                    logo.setAttribute('alt', team.name)
                }

                name.textContent = team.name
                system.textContent = team.system
                manager.textContent = team.manager
                captain.textContent = team.captain

                team.players.forEach((player, index) => {
                    const i = team.players.length - 1

                    if (team.players[index].identity === this.#positions[i - index].id) {

                        const playerPosition = this.#positions[i - index]
                        const [playerNumber, playerName] = playerPosition.querySelectorAll('span')
                        
                        playerNumber.textContent = `${player.number}`
                        playerNumber.style.backgroundColor = team.color
                        playerName.textContent = player.lastname
                        playerPosition.style.gridArea = player.zone
                    }
                })
            }
        })
    }

    /**
     * Sets the pitch according to the current team.
     * @param {Event} e A 'click' event.
     */
    setPitch(e) {
        this.#showMarks()
        this.#displayTeam()
        this.#showPlayer(e)
    }

    /**
     * Sets the entire text content of the current slide.
     * @param {HTMLButtonElement} languageBtn The default translation button.
     */
    setAllTextContent(languageBtn = document.querySelector('nav > button')) {
        const [title, desc1, desc2, arrowsElts] = this.#mainInfosContainer.children
        const [, teamInfos, playerInfosContainer] = this.#detailsInfosContainer.children
        const subtitle = arrowsElts.querySelector('h3')
        /** @type {NodeListOf<HTMLSpanElement>} */
        const [system, manager, captain] = teamInfos.querySelectorAll('ul li span')
        /** @type {NodeListOf<HTMLLIElement>} */
        const [playerName, playerPlace, playerBirth] = playerInfosContainer.querySelectorAll('ul li')
        const isNotFrench = languageBtn.textContent === 'fra'

        /**
         * 
         * @param {string} lang 
         * @param  {HTMLElement[]} els 
         */
        const setTeamDetails = (lang, ...els) => {
            for (const el of els) {
                el.parentNode.childNodes[0].textContent = this.#data.traduction[lang][el.dataset.name]
            }
        }

        /**
         * @param {'fra' | 'eng'} lang 
         */
        const handleLanguage = (lang = 'fra') => {
            title.textContent = this.#data.traduction[lang].title
            desc1.innerHTML = this.#data.traduction[lang].p1
            desc2.textContent = this.#data.traduction[lang].p2
            subtitle.textContent = this.#data.traduction[lang].h3
            setTeamDetails(lang, system, manager, captain)
        }
        
        isNotFrench ? handleLanguage('eng') : handleLanguage()

        if (playerName.textContent === 'Nom' || playerName.textContent === 'Name') {
            isNotFrench ? playerName.textContent = 'Name' : playerName.textContent = 'Nom' 
            isNotFrench ? playerPlace.textContent = 'Place' : playerPlace.textContent = 'Poste' 
            isNotFrench ? playerBirth.textContent = 'Date of birth' : playerBirth.textContent = 'Date de naissance'

        } else if (playerName.textContent !== 'Nom' && playerName.textContent !== 'Name') {
            const playerContainer = this.#detailsInfosContainer.querySelector('div:nth-child(3)')
            
            if (
                'dataset' in playerContainer &&
                typeof playerContainer.dataset === 'object' &&
                'num' in playerContainer.dataset
            ) {
                const index = +(playerContainer.dataset.num) - 1
                const teamIndex = () => {
                    switch(this.#detailsInfosContainer.querySelector('h3').textContent) {
                        case 'Paris-Saint-Germain':
                            return 1
    
                        case 'Olympique de Marseille':
                            return 2
    
                        default:
                            return 0
                    }
                }
                
                const { place, poste, year, month, day } = this.#data.teams[teamIndex()].players[index]
    
                if (isNotFrench) {
                    playerPlace.textContent = place
                    playerBirth.textContent = `Born in ${this.#getBirthDate([year, month, day], 'en-EN')}`
                
                } else {
                    playerPlace.textContent = poste
                    playerBirth.textContent = `Né le ${this.#getBirthDate([year, month, day])}`
                }
            }
        }
    }
}