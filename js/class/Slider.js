import { dt } from '../utils/handlers.js'
import * as Type from '../types/types.js'

export class Slider {
    /** @type {HTMLDivElement} */
    element
    /**
     * Creates a `local slider` into the current section.
     * @param {`#${string}`} selector The query string.
     * @param {number} [length] The number of slides.
     */
    constructor(selector, length) {
        this.element = document.querySelector(selector)
        this.length = length ?? this.element.children.length
        this.index = 0
    }

    /**
     * Gets the navigation buttons of the slider.
     * @returns An array of the slider buttons navigation.
     */
    #getNextAndPrevBtns() {
        if (this.element.parentNode instanceof HTMLDivElement) {
            const btns = this.element.parentNode.nextElementSibling
            const [prev, , next] = btns.querySelectorAll('li')
            return [prev, next]
        }
    }

    /**
     * Move `slider` and handles `class` name of the slider's buttons (previous and next).
     * @param {string} className
     * @param {HTMLLIElement | HTMLButtonElement} prev  
     * @param {HTMLLIElement | HTMLButtonElement} next  
     */
    #moveAndChangeBtnsDisplay(className, prev, next) {
        this.move()
        this.changeBtnsDisplay(className, prev, next)
    }

    /**
     * Add or remove the `class` name of the slider's buttons (previous and next).
     * @param {string} className
     * @param {HTMLLIElement | HTMLButtonElement} prev  
     * @param {HTMLLIElement | HTMLButtonElement} next  
     */
    changeBtnsDisplay(className, prev, next) {
        switch (this.index) {
            case 0:
                prev.classList.add(className)
                if (next.classList.contains(className)) next.classList.remove(className)
                break

            case this.length - 1:
                next.classList.add(className)
                if (prev.classList.contains(className)) prev.classList.remove(className)
                break
                
            default:
                if (prev.classList.contains(className)) prev.classList.remove(className)
                if (next.classList.contains(className)) next.classList.remove(className)
        }
    }

    move() {
        const slideWidth = this.element.clientWidth / this.length
        this.element.style.transform = `translateX(-${slideWidth * this.index}px)`
    }

    /**
     * Moves the slider back.
     * @param {string} className
     * @param {HTMLLIElement | HTMLButtonElement} prev  
     * @param {HTMLLIElement | HTMLButtonElement} next  
     */
    moveBack(
        className = 'hidden',
        prev = this.#getNextAndPrevBtns().at(0),
        next = this.#getNextAndPrevBtns().at(1)
    ) {
        if (this.index <= 0) return
        this.index--
        this.#moveAndChangeBtnsDisplay(className, prev, next)
    }
    
    /**
     * Moves the slider forward.
     * @param {string} className
     * @param {HTMLLIElement | HTMLButtonElement} prev  
     * @param {HTMLLIElement | HTMLButtonElement} next  
     */
    moveForward(
        className = 'hidden',
        prev = this.#getNextAndPrevBtns().at(0),
        next = this.#getNextAndPrevBtns().at(1)
    ) {
        if (this.index >= (this.length - 1)) return
        this.index++
        this.#moveAndChangeBtnsDisplay(className, prev, next)
    }
}


export class MainSlider extends Slider {
    #prev
    #next
    /** @type {Type.TitlesType['data']} */
    #data
    #title = document.querySelector('h1')
    /** @type {HTMLUListElement} */
    #landmarksContainer = document.querySelector('#main-container > span ul')

    /**
     * Creates the `main slider` based on the Slider class.
     * @param {`#${string}`} slider The query string.
     * @param {NodeListOf<HTMLButtonElement>} buttons .
     * @param {number | undefined} length The number of slides.
     */
    constructor(slider, buttons, length = undefined) {
        super(slider, length)
        this.#prev = buttons[0]
        this.#next = buttons[1]
        this.#data = dt.find(obj => obj.id === 'Titles').data
        this.#createLandmarks()
        this.#setYear()
        this.#prev.addEventListener('click', () => this.moveBack())
        this.#next.addEventListener('click', () => this.moveForward())
    }

    #createLandmarks() {
        while (this.#landmarksContainer.children.length < this.length) {
            const li = document.createElement('li')
            const btn = document.createElement('button')

            li.appendChild(btn)
            this.#landmarksContainer.appendChild(li)   
        }

        this.#landmarksContainer.querySelectorAll('li').forEach((landmark, index) => {
            
            landmark.querySelector('button').addEventListener('click', () => {
                const landmarks = [...this.#landmarksContainer.children]
                
                /**
                 * 1 - Add the active class to the corresponding landmark.
                 * 2 - Change the value of MainSlider index to the value of the corresponding index slide.
                 * 3 - Check for the display (or not) of the 'previous' & 'next' button.
                 * 4 - Change the 'title' text content.
                 */
                if (this.index !== index) {
                    landmarks.forEach(li => {
                        const activeBtn = li.querySelector('button')
                        
                        if (activeBtn.classList.contains('active')) {
                            activeBtn.classList.remove('active')
                        }
                        
                        landmarks[index].querySelector('button').classList.add('active')
                    })

                    this.index = index
                    super.move()
                    super.changeBtnsDisplay('none', this.#prev, this.#next)
                    this.#changeTitleTextContent()
                }
            })
        })

        this.#landmarksContainer.querySelector('button').classList.add('active')
    }

    /**
     * Fills the title according to the current slide and the active language.
     * @param {HTMLButtonElement} languageBtn The translation button. 
     */
    #changeTitleTextContent(languageBtn = document.querySelector('nav > button')) {
        switch(languageBtn.textContent.trim()) {
            case 'fra':
                this.#title.textContent = this.#data.traduction['eng'][this.index]
                break   
            
            default:
                this.#title.textContent = this.#data.traduction['fra'][this.index]
                break
        }

        this.#title.classList.add('fade')
        this.element.addEventListener('transitionend', () => this.#title.classList.remove('fade'))
    }

    /**
     * Sets the current year.
     */
    #setYear() {
        const year = document.querySelector('footer span:nth-child(2)')
        year.textContent = `${new Date().getFullYear()}`
    }

    /**
     * Sets the buttons display and the title text content according to the current slide.
     */
    #setSlideDisplay() {
        this.#changeTitleTextContent()
        this.#landmarksContainer.children[this.index].querySelector('button').classList.add('active')
    }

    /**
     * Sets the basic informations of the website and toggles the traduction `button` text content between 'french' and 'english'.
     * @param {HTMLButtonElement} languageBtn The translation button. 
     * @param {HTMLSpanElement} copyright The owner legal informations.
     * @param {NodeListOf<HTMLLIElement>} nav A list of indicators for the navigation though the slides.
     */
    setCopyrightAndArrowsNavTextContent(
        languageBtn = document.querySelector('nav > button'),
        copyright = document.querySelector('footer span'),
        nav = document.querySelectorAll('.slide > ul li:nth-child(2)')
    ) {
        const handleContent = (lang = 'fra', name = '© Dominique Talis') => {
            this.#title.textContent = this.#data.traduction[lang === 'eng' ? 'fra' : 'eng'][this.index]
            copyright.textContent = lang === 'eng'
            ? `Tous droits réservés ${name} `
            : `All rights reserved ${name} `
            nav.forEach(el => {
                el.textContent = lang === 'eng'
                ? 'Cliquez sur les flèches pour naviguer entre les slides'
                : 'Click on the arrows to navigate between the slides'
            })
            languageBtn.textContent = lang === 'fra' ? 'fra' : 'eng'
        }

        switch(languageBtn.textContent.trim()) {
            case 'fra':
                handleContent('eng')
                break
                
            default:
                handleContent()
        }
    }

    moveBack() {
        super.moveBack('none', this.#prev, this.#next)
        this.#setSlideDisplay()
        this.#landmarksContainer.children[this.index + 1].querySelector('button').classList.remove('active')
        
    }
    
    moveForward() {
        super.moveForward('none', this.#prev, this.#next)
        this.#setSlideDisplay()
        this.#landmarksContainer.children[this.index - 1].querySelector('button').classList.remove('active')
    }
}