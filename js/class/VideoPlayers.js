
export class VideoPlayers {
    /** @type {NodeListOf<HTMLDivElement>} */
    #videos
    
    /**
     * Creates a list of `video players`.
     * @param {`#${string}`} selector The query string.
     */
    constructor(selector) {
        this.#videos = document.querySelectorAll(selector)
    }

    fetchVideos() {
        this.#videos
        .forEach((container, key) => {
            /**
             * @param {string} id 
             */
            const insertEmbedVideo = (id) => `<iframe id="odysee-iframe" width="560" height="315" src="https://odysee.com/$/embed/@dominiquetalis:d/${id}?r=GaUTFYMYNse7YhRdJR1wvXhV4M2147dd" allowfullscreen></iframe>`
            const videoListLength = this.#videos.length

            switch(key) {
                case 0:
                    container.innerHTML = insertEmbedVideo('osange_talis_le_parcours_d_une_centenaire:7')
                    break

                default:
                    container.innerHTML = insertEmbedVideo('la_mobilite_au_sein_du_groupe_La_Poste:e')
            }

            container.nextElementSibling.textContent = `${key + 1}/${videoListLength}`
        })
    }
}