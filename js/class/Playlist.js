import { dt } from '../utils/handlers.js'
import * as Type from '../types/types.js'

export class Playlist {
    /** @type {HTMLDivElement} */
    #element
    /** @type {Type.PlaylistDataType} */
    #data
    /** @type {HTMLButtonElement[]} */
    #buttons
    #count

    /**
     * Creates a `playlist` handler.
     * @param {`#${string}`} selector The query string.
     */
    constructor(selector) {
        this.#element = document.querySelector(selector)
        this.#data = dt.find(obj => obj.id === 'Playlist').data
        this.#buttons = [
            ...this.#element
            .querySelector('div')
            .querySelectorAll('button')
        ]
        this.#count = 10 //number of tracks
    }

    /**
     * Inserts a new line in the playlist table.
     * @param {(string | number)[][]} tracklist The 90's tracks selected.
     * @param {number} i  
     */
    #insertNewLine(tracklist, i = 0) {
        const table = this.#element.querySelector('table').querySelector('tbody')
        /** @type {HTMLDivElement} */
        const playlistCover = this.#element.querySelector('div[name="cover"]')
        const newLine = table.insertRow(1)
        const playlistCoverWidth = playlistCover.clientWidth
        const rows = [...table.querySelectorAll('tr')]

        tracklist.at(this.#count)
        .forEach(cell => (
            newLine.insertCell(-1).textContent = typeof cell === 'string'
            ? cell
            : `${cell}`
        ))
        playlistCover.style.backgroundPosition = `-${playlistCoverWidth * this.#count}px 0`
        
        for (const row of rows) {
            if (i > 0) {
                row.addEventListener('click', () => {
                    if (rows.length === 11) {
                        playlistCover.style.backgroundPosition =
                            `-${playlistCoverWidth * (i - 1)}px 0`
                    }
                })
            }
        }
    }

    /**
     * Removes all the rows in the body of the table.
     * @param {HTMLTableSectionElement} tbody The table body.
     */
    #removeRows(tbody = this.#element.querySelector('table tbody')) {
        for (let i = 0; i < 11; i++) {
            let j = 1

            if (i > 0) {
                if (i >= j) {
                    tbody.removeChild(tbody.children[j])
                }
            }
        }
    }

    /**
     * Handles the behavior of the ranking.
     */
    handleRanking() {
        this.#count--

        if (this.#count < 0) return
        if (this.#count < 1) {
            const rankingBtn = this.#buttons.at(0)
            
            this.#insertNewLine(this.#data.playlist)

            rankingBtn.style.background = 'red'
            rankingBtn.style.cursor = 'not-allowed'
            
            if (rankingBtn.nextElementSibling instanceof HTMLSpanElement) {
                rankingBtn.nextElementSibling.style.display = 'flex'
            }

            this.#setBtnTextContent()
            
        } else this.#insertNewLine(this.#data.playlist)
    }

    /**
     * Handles the behavior of the reload.
     */
    reloadRanking() {
        const [rankingBtn, reloadBtn] = this.#buttons
        /** @type {HTMLDivElement} */
        const cover = this.#element.querySelector('div[name="cover"]')

        this.#count = 10

        cover.style.backgroundPosition = '-1700px 0'
        rankingBtn.style.background = 'var(--sky-blue)'
        rankingBtn.style.cursor = 'pointer'
        
        this.#setBtnTextContent()
        this.#removeRows()

        if (reloadBtn.parentNode instanceof HTMLSpanElement) {
            reloadBtn.parentNode.style.display = 'none'
        }
    }

    /**
     * Switches HTML selected elements content in the current query language `French or English`.
     * @param {HTMLButtonElement} languageBtn 
     */
    #setBtnTextContent(languageBtn = document.querySelector('nav > button')) {
        const lang = languageBtn.textContent === 'fra' ? 'eng' : 'fra'
        const [rankingBtn, reloadBtn] = this.#buttons

        reloadBtn.parentNode.childNodes[0].textContent = this.#data.traduction[lang].load
        this.#count === 0 
            ? rankingBtn.textContent = this.#data.traduction[lang].button.at(1)
            : rankingBtn.textContent = this.#data.traduction[lang].button.at(0)
    }

    /**
     * Switches HTML selected elements content in the current query language `French or English`.
     * @param {HTMLButtonElement} languageBtn 
     */
    setAllTextContent(languageBtn = document.querySelector('nav > button')) {
        const playlistInfos = this.#element
        .querySelector('div:first-child > div > div')
        
        const title = playlistInfos.querySelector('h2')
        const desc = playlistInfos.querySelector('p')
        const tableTitle = this.#element.querySelector('h3')
        
        /** @type {NodeListOf<HTMLTableCellElement>} */
        const [track, artist, album, year] = this.#element.querySelectorAll('th:not(:first-child)')

        /**
         * @param {'fra' | 'eng'} lang 
         * @param {(HTMLParagraphElement | HTMLHeadingElement | HTMLTableCellElement)[]} els 
         */
        const setPlaylistDetails = (lang, ...els) => {
            for (const el of els) {
                el.textContent = this.#data.traduction[lang][el.dataset.music]
            }
        }

        /**
         * @param {'fra' | 'eng'} lang 
         */
        const handleLanguage = (lang = 'fra') => {
            setPlaylistDetails(lang, title, desc, tableTitle, track, artist, album, year)
            this.#setBtnTextContent()
        }

        languageBtn.textContent === 'fra' ? handleLanguage('eng') : handleLanguage()
    }
}