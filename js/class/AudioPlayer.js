import { displayAudiusTimer } from '../utils/timer.js'
import * as Type from '../types/types.js'

export class AudioPlayer {
    /** @type {HTMLElement} */
    #container
    /** @type {HTMLSpanElement} */
    #infosWarning
    /** @type {HTMLSpanElement} */
    #screen
    /** @type {Function} */
    #timer
    /** @type {HTMLMediaElement} */
    #audio
    #index = 0
    #start = 0
    #id = 'k4zP3'

    /** @type {Type.DataType[]} */
    #tracks
    
    /**
     * Creates an`audio player`.
     * @param {`#${string}`} id 
     */
    constructor(id) {
        this.#container = document.querySelector(id)
        this.#infosWarning = document.querySelector(id + ' > span')
        this.#screen = this.#container.children[1].querySelector('span')
        this.#timer = displayAudiusTimer
        this.#audio = new Audio()
    }

    /**
     * @param {string} params 
     */
    #api(params) {
        return `https://discoveryprovider.audius.co/v1/${params}`
    }
    
    /**
     * @param {string} text 
     */
    #capitalizeFirstLetter(text) {
        if (
            text === String(text).toUpperCase() ||
            text === String(text).toLowerCase()
        ) {
            return text
            .split(' ')
            .reduce((str, word, i, arr) => {
                word = (
                    word.charAt(0).toUpperCase() +
                    word.slice(1).toLowerCase()
                )

                i !== arr.length - 1
                ? str += `${word} `
                : str += word
    
                return str
            }, '')
        }

        return text
    }

    /**
     * Reload the player.
     * @param {HTMLButtonElement} playTrack 
     */
    #reloadPlayer(
        playTrack = this.#container
        .querySelector('button[name="play track"]')
    ) {
        playTrack.classList.contains('play')
        ? playTrack.classList.remove('play')
        : null;

        this.#infosWarning.textContent
        ? this.#infosWarning.textContent = ''
        : null;
        
        this.#displayTrackInfos()
    }

    /**
     * Shows a message to the user that he/she cannot go futher, because it's start point or end point of the playlist. 
     * @param {HTMLButtonElement} traductionBtn The default translation button.
     */
    #showWarningInfos(
        traductionBtn = document.querySelector('nav > button')
    ) {
        /**
         * Returns alert text to the user.
         * @param {'beginning' | 'end' | 'au début' | 'à la fin'} position 
         * @returns 
         */
        const getText = (position) => {
            return position === 'end'|| position === 'beginning'
            ? `You are at the ${position} of the playlist !`
            : `Vous êtes ${position} de la playlist !`
        }

        switch (this.#index) {
            case this.#tracks.length - 1:
                this.#infosWarning.textContent = traductionBtn.textContent === 'fra'
                ? getText('end')
                : getText('à la fin')
                break
                
            case 0:
                this.#infosWarning.textContent = traductionBtn.textContent === 'fra'
                ? getText('beginning')
                : getText('au début')
                break
        }
    }
        
    /**
     * The main point of this player. It manages the retrieval of the track's url, the display of the thumbnail and the title. 
     */
    async #displayTrackInfos() {
        const track = this.#tracks.at(this.#index)
        const artist = this.#capitalizeFirstLetter(track.user.name)
        const title = this.#capitalizeFirstLetter(track.title)
        const getTrackUrl = await fetch(this.#api(`tracks/${track.id}/stream`))
        
        if (getTrackUrl.ok) {
            const cover = this.#container.querySelector('img')
    
            this.#audio.src = getTrackUrl.url
            this.#screen.innerHTML = `${artist} - <b>${title}</b>`
            cover.src = track.artwork['150x150']

        } else this.#displayErrorMsgToUser()
    }

    /**
     * @param {HTMLButtonElement} traductionBtn The default translation button.
     */
    #displayErrorMsgToUser(traductionBtn = document.querySelector('nav > button')) {
        this.#screen.textContent = traductionBtn.textContent === 'fra'
            ? 'No data available !'
            : 'Aucune donnée disponible !'
    }

    /**
     * At first, it check the 'readyState' property of the `audio` and the private 'start' property of the class (to avoid DOMException user agent).
     * If the value of 'readyState' is greater than zero, the function manages the timer's display and the condition to play or pause the audio.
     * @param {Event | null} e A 'click' event or an HTML 'button' element.
     * @param {HTMLSpanElement} timeTrack The timer display.
     */
    #play(
        e = null,
        timeTrack = this.#container.querySelector('div span')
    ) {
        if (this.#audio.readyState >= 1 && this.#start > 0) {
            this.#audio.addEventListener('timeupdate', () => {
                this.#timer(timeTrack, this.#audio)
            })

            /** @type {HTMLButtonElement} */
            let playBtn

            (e &&
            'type' in e &&
            e.type === 'click' &&
            e.currentTarget instanceof HTMLButtonElement)
            ? playBtn = e.currentTarget
            : playBtn = this.#container.querySelector('button[name="play track"]')

            if (this.#audio.paused) {
                this.#audio.play()
                playBtn.classList.add('play')
                
            } else {
                this.#audio.pause()
                playBtn.classList.remove('play')
            }
            
            if (this.#infosWarning.textContent) this.#infosWarning.textContent = ''
        }

        //If it's the first call of the function, assign 1 to the '#start' property.
        if (this.#start === 0) {
            this.#start = 1
        }
    }

    /**
     * Handles the condition to play or pause the audio.
     * @param {Event} e A 'click' event.
     */
    playTrackHandler(e) {
        this.#play(e)
    }

    /**
     * Shows a 'warning info message' if the index is less or equal to 0. Otherwise decrement the index and load the previous track.
     */
    prevTrackHandler() {
        if (this.#index <= 0) {
            this.#showWarningInfos()
            return
    
        } else {
            this.#index--
            this.#reloadPlayer()
        }
    }

    /**
     * Shows a 'warning info message' if the index is greater or equal to 0. Otherwise increment the index and load the next track.
     */
    nextTrackHandler() {
        if (this.#index < this.#tracks.length - 1) {
            this.#index++
            this.#reloadPlayer()
    
        } else {
            this.#showWarningInfos()
            return
        }
    }

    /**
     * Fetchs the playlist data from the 'Audius' api.
     */
    async fetchDataFromAudiusApi() {
        try {
            const res = await fetch(this.#api(`playlists/${this.#id}/tracks`))
    
            if (res.ok) {
                const { data } = await res.json()
                
                this.#tracks = data
                await this.#displayTrackInfos()

                this.#audio.addEventListener('canplay', () =>  {
                    this.#timer(this.#container.querySelector('div span'), this.#audio)
                    this.#play()
                })
                this.#audio.addEventListener('ended', () => {
                    if (this.#index < this.#tracks.length - 1) {
                        this.#index++
                        this.#reloadPlayer()
                    }
                })
            } else {
                this.#displayErrorMsgToUser()
            }
        } catch (error) {
            console.error(error.message)
            this.#displayErrorMsgToUser()
        }
    }
}