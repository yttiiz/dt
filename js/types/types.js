//====================| Audio types |/====================//

/**
 * @typedef {Object} AudiusSdkType
 * @property {Record<'getPlaylistTracks', PlaylistTrackFuncType>} playlists
 * @property {Record<'streamTrack', StreamTrackFuncType>} tracks
 */

/**
 * @callback PlaylistTrackFuncType
 * @param {PlaylistId} arg
 * @returns {Promise<Record<'data', DataType[]>>}
 */

/**
 * @typedef {Record<string, string> & { user: { name: string } } & { artwork: string }} DataType
 */

/**
 * @callback StreamTrackFuncType
 * @param {TrackId} arg
 * @returns {Promise<string>}
 */

/**
 * @typedef {Object} PlaylistId 
 * @property {string} playlistId
 */

/**
 * @typedef {Object} TrackId 
 * @property {string} trackId
 */

//====================| Json types |/====================//

/** @typedef {DataSlideType<string[]>} TitlesType */
/** @typedef {DataSlideType<PresentationInfoType>} PresentationDataType */
/** @typedef {DataSlideType<CareerInfoType>} CareerDataType */
/** @typedef {DataSlideType<SkillsInfoType>} SkillsDataType */
/** @typedef {DataSlideType<TeamsTraductionType> & TeamsDataType} PitchDataType */
/** @typedef {DataSlideType<PlaylistTraductionType> & PlaylistInfo} PlaylistDataType */
/** @typedef {DataSlideType<FormTraductionType>} FormDataType */

// Presentation data

/** @typedef {Record<KeyOfTraductionType, (string | string[])[]>} PresentationInfoType */

// Career data

/** @typedef {Record<KeyOfTraductionType, Record<'imagine' | 'yz' | 'laposte' | 'matacapital', string[]>>} CareerInfoType */

// Skills data

/** @typedef {Record<KeyOfTraductionType, string[]>} SkillsInfoType */

// Playlist data

/** @typedef {Record<'playlist', (string | number)[][]>} PlaylistInfo */

/**
 * @typedef {Object} PlaylistTraductionType
 * @property {string} title
 * @property {string} p
 * @property {string} h3
 * @property {string} track
 * @property {string} artist
 * @property {string} album
 * @property {string} year
 * @property {string[]} button
 * @property {string} load
 */

// Pitch data

/** @typedef {Record<'teams', TeamsType[]>} TeamsDataType */

/**
 * @typedef {Object} TeamsTraductionType
 * @property {string} title
 * @property {string} p1
 * @property {string} p2
 * @property {string} h3
 * @property {string} system
 * @property {string} manager
 * @property {string} captain
 */

/**
 * @typedef {Object} TeamsType
 * @property {string} name
 * @property {string} logo
 * @property {string} color
 * @property {string} system
 * @property {string} manager
 * @property {string} captain
 * @property {PlayerType[]} players
 */

/**
 * @typedef {Object} PlayerType
 * @property {string} identity
 * @property {number} number
 * @property {string} lastname
 * @property {string} firstname
 * @property {number} day
 * @property {number} month
 * @property {number} year
 * @property {string} poste
 * @property {string} place
 * @property {string} nationalite
 * @property {string} photo
 * @property {`${string} / ${string} / ${string} / ${string}`} zone
 */

// Form data

/** 
 * @typedef {Object} FormTraductionType
 * @property {string} text
 * @property {string} lastname
 * @property {string} firstname
 * @property {string} website
 * @property {string} message
 * @property {string} button
 * @property {string} fieldsRequired
 * @property {string} successMessage
 * @property {string} errorRequestMessage
 * @property {string} errorRequiredMessage
 * @property {string} errorCharacterMessage
 * @property {string} errorRecaptchaMessage
 */

// basics types

/** @typedef {'fra' | 'eng'} KeyOfTraductionType */

/** @typedef {'Titles' | 'Presentation' | 'Career' | 'Skills' | 'Playlist' | 'Pitch' | 'Form'} KeyOfIdType */

/**
 * @template T
 * @typedef {{ id: KeyOfIdType; data: Record<'traduction', TraductionSlideType<T>>;}} DataSlideType
*/

/**
 * @template T
 * @typedef {Record<KeyOfTraductionType, T>} TraductionSlideType
 */

export {}